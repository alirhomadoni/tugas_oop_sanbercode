<?php 
	/*KELAS UTAMA */
	trait Hewan{
		public $darah= 50.0;
		public $jumlahKaki, $keahlian,$nama;

		public function atraksi(){
			echo "{$this->nama} sedang {$this->keahlian} <br>";

		}
	}

	abstract Class Fight {
		use Hewan;
		public $attackPower ;
		public $deffencePower ;

		public function serang($lawan){
			echo "{$this->nama} sedang menyerang {$lawan->nama}..... <br>"; 
		}
		public function diserang($lawan){
			echo "{$lawan->nama} sedang diserang {$this->nama}..... <br>";
			$this->darah = ($this->darah - ($lawan->attackPower/$this->deffencePower));
		}
	
	}
	/*Child Class*/ 
	class  Elang extends Fight {
		public $jumlahKaki=2;
		public $keahlian = "terbang tinggi";

		public function __construct ($nama){
			$this->attackPower = 10;
			$this->deffencePower =5;
			$this->nama=$nama;
		}
		public function getInfoHewan(){
			$namaKelas= get_class($this);
		
			echo "Nama Hewan        : {$this->nama} <br>";
			echo "Nama Jumlah Kaki  : {$this->jumlahKaki} <br>";
			echo "Darah             : {$this->darah} <br>";
			echo "Keahlian          : {$this->keahlian} <br>";
			echo "Attack Power      : {$this->attackPower} <br>";
			echo "Deffence Power    : {$this->deffencePower} <br>";
			echo "Kelas            : {$namaKelas} <br>";

	}

}
	class Harimau Extends Fight {
		public $jumlahKaki=4;
		public $keahlian = "lari cepat";
		public function __construct ($nama){
			$this->attackPower = 8;
			$this->deffencePower =7;
			$this->nama=$nama;
		}
		public function getInfoHewan(){
			$namaKelas= get_class($this);
			
			echo "Nama Hewan        : {$this->nama} <br>";
			echo "Nama Jumlah Kaki  : {$this->jumlahKaki} <br>";
			echo "Darah             : {$this->darah} <br>";
			echo "Keahlian          : {$this->keahlian} <br>";
			echo "Attack Power      : {$this->attackPower} <br>";
			echo "Deffence Power    : {$this->deffencePower} <br>";
			echo "Kelas            : {$namaKelas} <br>";
		}
	}
 ?>		
